import React, { useCallback, useEffect } from "react";
import { useState } from "react";
import { useHash } from "react-use";
import styled from "styled-components";
import {
  IoArrowUpSharp,
  IoArrowDownSharp,
  IoSaveSharp,
  IoDesktopOutline,
  IoFilmOutline,
  IoVolumeHighSharp,
  IoTelescopeOutline,
  IoTelescopeSharp,
  IoSearchOutline,
} from "react-icons/io5";

const AppStyled = styled.div`
  padding: 20px;
`;

const SearchRow = styled.div`
  width: 100%;
  display: flex;
`;

const SearchField = styled.input`
  font-size: 28px;
  padding: 10px 20px;
  flex: 4;
  background: white;
  color: #222222;
  border: 1px black;
  outline: none;
`;

const SearchButton = styled.button`
  padding: 20px 20px;
  background: gray;
  border: none;
  outline: none;

  :hover {
    background: #b4b4b4;
  }
`;

const Results = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`;

const ResultRow = styled.div`
  padding: 10px;
  margin-top: 10px;
  background: white;
  color: black;
  width: 100%;
  display: flex;
  flex-direction: column;
  cursor: pointer;
  :hover {
    background: #d4d4d4;
  }
`;

export const ResultRowHead = styled.div`
  width: 100%;
  display: flex;

  div {
    padding-left: 5px;
    align-items: center;
    display: flex;
    svg {
      margin-right: 3px;
    }
  }
`;

const showRange = (values: number[]) => {
  const ranges: string[] = [];

  let start: number;
  values.forEach((val, i) => {
    if (!start) {
      start = val;
      return;
    }

    const prev = values[i - 1];

    if (start) {
      if (val - prev !== 1) {
        ranges.push(`${start}-${prev}`);
      }
    }
  });

  // @ts-ignore
  if (start) {
    const lastValues = values[values.length - 1];

    if (lastValues !== start) {
      ranges.push(`${start}-${lastValues}`);
    } else {
      ranges.push(`${lastValues}`);
    }
  }

  return ranges.join(", ");
};

enum Loading {
  IDLE,
  PROGRESS,
  SUCCESS,
}

function App() {
  const [hash, setHash] = useHash();
  const [isLoading, setLoadingState] = useState(false);
  const [result, setResult] = useState([]);
  const [expandedIdx, setExpandedId] = useState<number>();
  const value = decodeURI(hash.substr(1));
  const [season, setSeason] = useState<number>();
  const [episode, setEpisode] = useState<number>();
  const [isInfoLoading, setInfoLoading] = useState(Loading.IDLE);
  const [files, setFiles] = useState<string[]>([]);
  const [_magnet, setMagnet] = useState<string>();
  const [videoUrl, setVideoUrl] = useState<string>();

  const getMagnet = async (magnetToken: string) => {
    const { magnet } = await fetch(
      `http://io.sawa.cat/magnet/${magnetToken}`
    ).then((d) => d.json());
    return magnet;
  };

  const getInfo = async (magnetToken: string) => {
    setInfoLoading(Loading.PROGRESS);
    const magnet = await getMagnet(magnetToken);
    const data = await fetch(
      `http://io.sawa.cat/info?magnet=${magnet}`
    ).then((d) => d.json());
    setFiles(data.files.map((f: any) => f.name));

    setInfoLoading(Loading.SUCCESS);

    return magnet;
  };

  const getVideoLink = (magnet: string, file: string) => {
    return `http://io.sawa.cat/watch?magnet=${magnet}&name=${file}`;
  };

  const magnet = async (row: any) => {
    window.open(await getMagnet(row.magnetToken));
  };

  const expand = (idx: number) => {
    setExpandedId(idx);
    setMagnet(undefined);
    setInfoLoading(Loading.IDLE);
    setFiles([]);
    setVideoUrl(undefined);
  };

  const watch = async (row: any) => {
    const magnet = await getInfo(row.magnetToken);
    setMagnet(magnet);
  };

  const video = async (row: any) => {
    const magnet = await getInfo(row.magnetToken);
    setMagnet(magnet);
  };

  const fetchData = async () => {
    setLoadingState(true);
    const data = await fetch(
      `http://io.sawa.cat/search/${encodeURI(value)}`
    ).then((d) => d.json());

    if (!data.statusCode) {
      setResult(data);
    }
    setLoadingState(false);
  };

  const submit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    fetchData();
  };

  const player = (url: string) => {
    setVideoUrl(url);
  };

  const updateFilter = useCallback(() => {
    const seasonRes = /(сезон[ ]*[:]?[ ]*(\d+)|(\d+)[ ]*сезон|(S\d+))/gim.exec(
      value
    );

    const inTextSeason =
      seasonRes && (+seasonRes[2] || +seasonRes[3] || +seasonRes[0].substr(1));

    setSeason(inTextSeason || undefined);

    const episodeRes = /(серия[ ]*[:]?[ ]*(\d+)|(\d+)[ ]*серия|(E\d+))/gim.exec(
      value
    );
    const inTextEpisode =
      episodeRes &&
      (+episodeRes[2] || +episodeRes[3] || +episodeRes[0].substr(1));

    setEpisode(inTextEpisode || undefined);
  }, [season, episode, value]);

  useEffect(() => {
    if (value) {
      fetchData();
    }
  }, []);

  useEffect(() => {
    updateFilter();
  }, [value]);

  const rows = result
    .filter(
      (row: any) =>
        (!season || row.media.seasons.includes(season)) &&
        (!episode || row.media.episodes.includes(episode))
    )
    .filter((row: any) => !row.title.includes("lossless"))
    .filter((row: any) => !row.title.includes("(OST)"))
    .filter(
      ({ media }: any) =>
        !(
          !media.audio &&
          !media.code &&
          !media.container &&
          !media.quality &&
          !media.resolution &&
          media.episodes.length === 0 &&
          media.seasons.length === 1 &&
          media.seasons[0] === null
        )
    );

  return (
    <AppStyled>
      <form onSubmit={submit}>
        <SearchRow>
          <SearchField
            autoFocus
            defaultValue={value}
            placeholder="Что хотим посмотреть?"
            onInput={(e) => setHash((e.target as any).value)}
          />
          <SearchButton type="submit">Найти</SearchButton>
        </SearchRow>
      </form>
      <Results>
        {isLoading && (
          <ResultRow>
            <ResultRowHead>
              <IoSearchOutline /> Ищем...
            </ResultRowHead>
          </ResultRow>
        )}
        {!isLoading && value && (
          <ResultRow>
            Показывать только: {`${episode || "любую"} серию`} в{" "}
            {`${season || "любом"} сезоне`}
          </ResultRow>
        )}
        {!isLoading && rows.length === 0 && value && (
          <ResultRow>Кажется ничего не найдено (:</ResultRow>
        )}
        {!isLoading &&
          rows.map((row: any, i) => (
            <ResultRow key={row.magnetToken} onClick={() => expand(i)}>
              <ResultRowHead>
                <div>
                  <IoArrowUpSharp color="green" />
                  {row.seeders}
                </div>
                <div>
                  <IoArrowDownSharp color="red" /> {row.leechers}
                </div>
                <div>
                  <IoVolumeHighSharp color="#795548" />{" "}
                  {row.media.voice?.join(", ") || "хз"}
                </div>
                <div>
                  <IoFilmOutline color="#2196f3" /> {row.media.quality || "хз"}
                </div>
                <div>
                  <IoDesktopOutline />
                  {row.media.resolution || "хз"}
                </div>
                <div>
                  <IoSaveSharp color="gray" />
                  {row.size}
                </div>
                <div>
                  <IoTelescopeOutline color="rgb(76 175 76)" />
                  Сезоны: {showRange(row.media.seasons) || "хз"}
                </div>
                <div>
                  <IoTelescopeSharp color="rgb(76 175 76)" />
                  Серии: {showRange(row.media.episodes) || "хз"}
                </div>
              </ResultRowHead>
              {expandedIdx === i && (
                <div style={{ padding: 5 }}>
                  <div>{row.title}</div>
                  <div style={{ padding: 5 }}>
                    <button
                      style={{ padding: 5, marginLeft: 5 }}
                      onClick={() => magnet(row)}
                    >
                      Открыть magnet
                    </button>
                    <button
                      style={{ padding: 5, marginLeft: 5 }}
                      onClick={() => video(row)}
                      disabled={isInfoLoading === Loading.PROGRESS}
                    >
                      Получить ссылку на стрим видео
                    </button>
                    <button
                      style={{ padding: 5, marginLeft: 5 }}
                      onClick={() => watch(row)}
                      disabled={isInfoLoading === Loading.PROGRESS}
                    >
                      Смотреть в плеере
                    </button>
                  </div>
                  {isInfoLoading === Loading.PROGRESS && (
                    <div>Получение списка файлов</div>
                  )}
                  {isInfoLoading === Loading.SUCCESS && (
                    <div>
                      <div>.avi, некоторые .mkv не поддерживаются в плеере</div>
                      Список файлов:
                      {files.map((name) => (
                        <div>
                          {name}
                          <button
                            style={{ padding: 5, marginLeft: 5 }}
                            onClick={() =>
                              window.open(getVideoLink(_magnet!, name))
                            }
                          >
                            Получить ссылку на стрим видео
                          </button>
                          <button
                            style={{ padding: 5, marginLeft: 5 }}
                            onClick={(e) => {
                              e.preventDefault();
                              e.stopPropagation();
                              player(getVideoLink(_magnet!, name));
                            }}
                          >
                            Смотреть в плеере
                          </button>
                          {videoUrl && videoUrl.includes(name) && (
                            <div>
                              <video
                                id="video"
                                width="500"
                                controls
                                src={videoUrl}
                                autoPlay
                                // @ts-ignore
                                type="video/mp4"
                              >
                                Your browser does not support the video tag.
                              </video>
                            </div>
                          )}
                        </div>
                      ))}
                    </div>
                  )}
                </div>
              )}
            </ResultRow>
          ))}
      </Results>
    </AppStyled>
  );
}

export default App;
